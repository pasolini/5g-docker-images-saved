# 5G docker images case study saved

## TODO pre-start
- install docker

## How to start
```bash
sudo ./boostrap.sh
sudo ./run.sh
```
## How to stop
```bash
cd ./ci-scripts/yaml_files/5g_rfsimulator
sudo docker compose down
```

## What is going to run
1. **boostrap.sh**: just load in the local docker (without using internet) the images
2. **run.sh**: start the 5G Core Network using the yaml file inside ci-sripts/yaml_files/5g_rfsimulator

## List images saved
    oai-nr-ue:latest 
    oai-gnb:develop
    oaisoftwarealliance/oai-spgwu-tiny:v1.5.0
    oaisoftwarealliance/oai-smf:v1.5.0
    oaisoftwarealliance/oai-amf:v1.5.0
    mysql:8.0 
    oaisoftwarealliance/oai-nrf:v1.5.0
    oaisoftwarealliance/trf-gen-cn5g:latest

## Command to import docker images locally
    docker load --input ....tar
