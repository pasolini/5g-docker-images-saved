#!/bin/sh -eux

docker load --input mysql:8.0.tar 
docker load --input oai-amf:v1.5.0.tar 
docker load --input oai-gnb:develop.tar 
docker load --input oai-nr-ue:latest.tar 
docker load --input oai-nrf:v1.5.0.tar 
docker load --input oai-smf:v1.5.0.tar 
docker load --input oai-spgwu-tiny:v1.5.0.tar 
docker load --input trf-gen-cn5g:latest.tar 
