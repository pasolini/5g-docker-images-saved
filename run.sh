cd ./ci-scripts/yaml_files/5g_rfsimulator
docker compose up -d mysql oai-nrf oai-amf oai-smf oai-spgwu oai-ext-dn

# Wait for all 6 containers to be in a healthy state
while [ 1 ]; do
    docker compose ps | grep healthy | wc -l | grep 6 && break
    sleep 1s
done

docker compose up -d oai-gnb

# Wait for all 7 containers to be in a healthy state
while [ 1 ]; do
    docker compose ps | grep healthy | wc -l | grep 7 && break
    sleep 1s
done

sleep 3s

docker compose up -d oai-nr-ue

# Wait for all 8 containers to be in a healthy state
while [ 1 ]; do
    docker compose ps | grep healthy | wc -l | grep 8 && break
    sleep 1s
done
